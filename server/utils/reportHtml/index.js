const defaultTheme = require('./defaultTheme.js');

function json_format(json) {
  if (json && typeof json === 'object') {
    return JSON.stringify(json, null, '   ');
  }
  return json;
}

module.exports = function renderToHtml(reports) {
  let tp = createHtml(reports);
  return tp;
};

function createHtml(reports) {
  let mdTemplate = ``;
  let left = ``;
  reports.list.map((item, index) => {
    mdTemplate += baseHtml(index, item.name, item.path, item.status);
    mdTemplate += validHtml(item.validRes);
    mdTemplate += requestHtml(item.url, item.headers, item.data);
    mdTemplate += reponseHtml(item.res_header, item.res_body);
    left += leftHtml(index, item.name, item.code);
    // left += codeHtml(item.code);
  });
  return createHtml5(left, mdTemplate, reports.message, reports.runTime);
}

function createHtml5(left, tp, msg, runTime) {
  let message = ``;
  if (msg.failedNum === 0) {
    message += `<div>一共 <span class="success">${
      msg.successNum
    }</span> 测试用例， 全部验证通过(${runTime})</div>`;
  } else {
    message += `<div>一共 ${msg.len} 测试用例，<span class="success"> ${
      msg.successNum
    }</span> 个验证通过， ${msg.failedNum} 个未通过(${runTime})</div>`;
  }

  //html5模板
  let html = `<!DOCTYPE html>
  <html>
  <head>
  <title>测试报告</title>
  <meta charset="utf-8" />
  ${defaultTheme}
  </head>
  <body class="yapi-run-auto-test">
    <div class="m-header">
<!--      <a href="#" style="display: inherit;"><svg class="svg" width="32px" height="32px" viewBox="0 0 64 64" version="1.1"><title>Icon</title><desc>Created with Sketch.</desc><defs><linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-1"><stop stop-color="#FFFFFF" offset="0%"></stop><stop stop-color="#F2F2F2" offset="100%"></stop></linearGradient><circle id="path-2" cx="31.9988602" cy="31.9988602" r="2.92886048"></circle><filter x="-85.4%" y="-68.3%" width="270.7%" height="270.7%" filterUnits="objectBoundingBox" id="filter-3"><feOffset dx="0" dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset><feGaussianBlur stdDeviation="1.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur><feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.159703351 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix></filter></defs><g id="首页" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="大屏幕"><g id="Icon"><circle id="Oval-1" fill="url(#linearGradient-1)" cx="32" cy="32" r="32"></circle><path d="M36.7078009,31.8054514 L36.7078009,51.7110548 C36.7078009,54.2844537 34.6258634,56.3695395 32.0579205,56.3695395 C29.4899777,56.3695395 27.4099998,54.0704461 27.4099998,51.7941246 L27.4099998,31.8061972 C27.4099998,29.528395 29.4909575,27.218453 32.0589004,27.230043 C34.6268432,27.241633 36.7078009,29.528395 36.7078009,31.8054514 Z" id="blue" fill="#2359F1" fill-rule="nonzero"></path><path d="M45.2586091,17.1026914 C45.2586091,17.1026914 45.5657231,34.0524383 45.2345291,37.01141 C44.9033351,39.9703817 43.1767091,41.6667796 40.6088126,41.6667796 C38.040916,41.6667796 35.9609757,39.3676862 35.9609757,37.0913646 L35.9609757,17.1034372 C35.9609757,14.825635 38.0418959,12.515693 40.6097924,12.527283 C43.177689,12.538873 45.2586091,14.825635 45.2586091,17.1026914 Z" id="green" fill="#57CF27" fill-rule="nonzero" transform="translate(40.674608, 27.097010) rotate(60.000000) translate(-40.674608, -27.097010) "></path><path d="M28.0410158,17.0465598 L28.0410158,36.9521632 C28.0410158,39.525562 25.9591158,41.6106479 23.3912193,41.6106479 C20.8233227,41.6106479 18.7433824,39.3115545 18.7433824,37.035233 L18.7433824,17.0473055 C18.7433824,14.7695034 20.8243026,12.4595614 23.3921991,12.4711513 C25.9600956,12.4827413 28.0410158,14.7695034 28.0410158,17.0465598 Z" id="red" fill="#FF561B" fill-rule="nonzero" transform="translate(23.392199, 27.040878) rotate(-60.000000) translate(-23.392199, -27.040878) "></path><g id="inner-round"><use fill="black" fill-opacity="1" filter="url(#filter-3)" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#path-2"></use><use fill="#F7F7F7" fill-rule="evenodd" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#path-2"></use></g></g></g></g></svg></a>-->
      <a href="#" style="display: inherit;"><svg t="1589005061029" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="9499" xmlns:xlink="http://www.w3.org/1999/xlink" width="32px" height="32px"><defs><style type="text/css"></style></defs><path d="M868 584.4c7.3 2.9 14.2 6.1 20.8 9.5 0.5-2.3 1.3-4.6 1.7-6.9 29.6-150-28.2-307-147.5-400.1-12.6-9.8-28.9-13.8-44.7-10.6-15.9 3.1-29.6 13-37.6 27.1-13.5 23.7-7.4 54.1 14 70.9C760.4 341.2 802 450 783.3 558.5c-0.5 2.9-1.5 5.6-2.1 8.5 8.1 0.1 16.5 0.6 25 1.7 20.9 2.7 42.4 8.2 61.8 15.7z" fill="#FB536C" p-id="9500"></path><path d="M888.8 593.9c-2.8 13.4-5.9 26.8-10.1 40-4.6 14.3-14.6 25.9-28.1 32.6-7.8 3.9-16.3 5.9-24.8 5.9-6.1 0-12.2-1-18.2-3.1-28.7-9.9-43.8-40.5-34.3-69.6 3.4-10.5 5.7-21.6 8-32.8-34.4-0.5-63 8.3-70.3 30.8-12.9 39.9 13.3 122 79.6 138.3 63.2 15.6 133.5-34.2 144.1-83.5 4.8-22.1-15.7-43-45.9-58.6z" fill="#FDBA3E" p-id="9501"></path><path d="M807.6 669.4c5.9 2.1 12.1 3.1 18.2 3.1 8.5 0 16.9-2 24.8-5.9 13.5-6.7 23.4-18.3 28.1-32.6 4.3-13.2 7.3-26.6 10.1-40-6.6-3.4-13.4-6.6-20.8-9.5-19.4-7.5-40.9-13.1-61.7-15.8-8.6-1.1-16.9-1.6-25-1.7-2.2 11.2-4.6 22.3-8 32.8-9.4 29.1 5.6 59.6 34.3 69.6z" fill="#8F3694" p-id="9502"></path><path d="M632.2 770.7c-100.7 40.7-215.7 22.3-300.3-48.1-2.3-1.9-4.1-4.1-6.4-6.1-4.2 7-8.7 14-14 20.8-12.7 16.8-28.2 32.7-44.5 45.7-6.1 4.9-12.4 9.3-18.6 13.2 1.8 1.6 3.3 3.4 5.1 5 115.1 100.7 280 129.1 420.2 72.3 14.8-6 26.4-18.1 31.5-33.4 5.2-15.3 3.5-32.1-4.7-46.1-13.7-23.5-43.1-33.5-68.3-23.3z" fill="#FB536C" p-id="9503"></path><path d="M204.6 726.8c0.5-8.7 3-17 7.3-24.4 3.1-5.3 7-10.1 11.8-14.2 23-19.9 57-17.7 77.4 5.1 7.4 8.2 15.9 15.8 24.4 23.3 17.6-29.6 24.3-58.8 8.5-76.3-28.1-31.1-112.3-49.5-159.6-0.2-45.1 47-37.1 132.7 0.2 166.6 16.8 15.3 45.1 7.9 73.8-10.4-10.2-9.2-20.3-18.5-29.6-28.8-10-11.2-15.1-25.6-14.2-40.7z" fill="#FDBA3E" p-id="9504"></path><path d="M311.5 737.4c5.3-6.9 9.8-13.8 14-20.8-8.6-7.5-17-15.1-24.4-23.3-20.5-22.7-54.5-24.9-77.4-5.1-4.8 4.1-8.7 8.9-11.8 14.2-4.2 7.4-6.8 15.6-7.3 24.4-0.9 15.1 4.1 29.4 14.2 40.6 9.3 10.3 19.4 19.6 29.6 28.8 6.2-4 12.5-8.3 18.6-13.2 16.3-13 31.8-28.9 44.5-45.6z" fill="#8F3694" p-id="9505"></path><path d="M216.8 490.5c15.1-107.6 88.5-198 191.8-236 2.8-1 5.6-1.5 8.4-2.5-4-7.1-7.7-14.5-11-22.5-8.1-19.4-14.1-40.8-17.2-61.3-1.2-7.8-1.8-15.3-2.2-22.7-2.2 0.7-4.6 1.2-6.9 1.9C235 196.7 128 325.3 107 475.1c-2.2 15.8 2.5 31.9 13.2 44 10.7 12.2 26 19.1 42.3 19 27.2-0.2 50.5-20.6 54.3-47.6z" fill="#FB536C" p-id="9506"></path><path d="M468.6 142.2c7.3 4.8 13.2 11.2 17.5 18.5 3.1 5.3 5.2 11.1 6.4 17.3 5.7 29.8-13.2 58.1-43.1 64.5-10.8 2.3-21.6 5.9-32.4 9.5 16.8 30 38.7 50.4 61.8 45.5 41-8.8 99-72.5 80-138.1-18.1-62.5-96.4-98.5-144.4-83.1-21.6 7-29.4 35.1-27.8 69.1 13-4.3 26.2-8.3 39.7-11.2 14.7-3.2 29.7-0.3 42.3 8z" fill="#FDBA3E" p-id="9507"></path><path d="M406 229.5c3.3 8 7.1 15.4 11 22.5 10.8-3.7 21.6-7.2 32.4-9.5 29.9-6.4 48.8-34.7 43.1-64.5-1.2-6.2-3.4-12-6.4-17.3-4.2-7.4-10.2-13.7-17.5-18.5-12.6-8.3-27.6-11.1-42.2-8-13.6 2.9-26.7 7-39.7 11.2 0.3 7.4 1 14.9 2.2 22.7 3 20.6 9 42 17.1 61.4z" fill="#8F3694" p-id="9508"></path></svg></a>
      <a href="#"><h1 class="title">工业APP开放接口标准化测试工具 测试结果文档</h1></a>
<!--      <div class="nav">-->
<!--        <a href="https://hellosean1025.github.io/yapi">YApi</a>-->
<!--      </div>-->
    </div>
    <div class="g-doc">
      <div class="menu-left">
        ${left}
      </div>
      <div id="right" class="content-right">
      <h1>工业APP开放接口标准化测试工具 测试报告</h1>
      <div class="summary">${message}</div>
      ${tp}
        <footer class="m-footer">
          <p>Build by <a href="https://ymfe.org/">YMFE</a>.</p>
        </footer>
      </div>
    </div>
  </body>
  </html>
  `;
  return html;
}

function requestHtml(url, headers, params) {
  headers = json_format(headers, null, '   ');
  params = json_format(params);
  let html = ``;
  html += `
  <div>
    <h3>Request</h3>
    <div class="row case-report">
     <div class="col-3 case-report-title">Url</div>
     <div class="col-21">${url}</div>
    </div>`;
  html += headers
    ? `<div class="row case-report">
    <div class="col-3 case-report-title">Headers</div>
    <div class="col-21">
     <pre>${headers}</pre>
    </div>
   </div>`
    : ``;

  html += params
    ? ` <div class="row case-report">
   <div class="col-3 case-report-title">Body</div>
   <div class="col-21">
    <pre>${params}</pre>
   </div>
   </div>`
    : ``;
  html += `</div>`;

  return html;
}

function reponseHtml(res_header, res_body) {
  res_header = json_format(res_header, null, '   ');
  res_body = json_format(res_body, null, '   ');
  let html = ``;
  html += `<div><h3>Reponse</h3>`;

  html += res_header
    ? `
  <div class="row case-report">
   <div class="col-3 case-report-title">Headers</div>
   <div class="col-21">
    <pre>${res_header}</pre>
   </div>
  </div>`
    : ``;

  html += res_body
    ? ` <div class="row case-report">
  <div class="col-3 case-report-title">Body</div>
  <div class="col-21">
   <pre>${res_body}</pre>
  </div>
 </div>`
    : ``;

  html += `</div>`;

  return html;
}

function validHtml(validRes) {
  if (validRes && Array.isArray(validRes)) {
    validRes = validRes.map((item, index) => {
      return `<div key=${index}>${item.message}</div>`;
    });
  }
  let html = `
  <div>
    <div class="row case-report">
     <div class="col-3 case-report-title">验证结果</div>
     <div class="col-21">
      ${validRes}
     </div>
    </div>
  </div>
  
  `;

  return html;
}

function baseHtml(index, name, path, status) {
  let html = `
  <div>
    <h2 id=${index}>${name}</h2>
    <h3>基本信息</h3>
    <div class="row case-report">
    <div class="col-3 case-report-title">Path</div>
    <div class="col-21">${path}</div>
   </div>
   <div class="row case-report">
    <div class="col-3 case-report-title">Status</div>
    <div class="col-21">${status}</div>
   </div>
  </div>
  `;

  return html;
}

function leftHtml(index, name, code) {
  let html = `
  <div class="list-content">
    <a class="list" href="#${index}">${name}</a>
    ${codeHtml(code)}
  </div>
  `;

  return html;
}

function codeHtml(code) {
  let codeHtml = ``;
  switch (code) {
    case 0:
      codeHtml += `<div title="验证通过" class="status status-ok"><i class="icon icon-check-circle"></i></div>`;
      break;
    case 400:
      codeHtml += `<div title="请求异常" class="status status-ko"><i class="icon icon-close-circle"></i></div>`;
      break;
    case 1:
      codeHtml += `<div title="验证失败" class="status status-warning"><i class="icon icon-warning-circle"></i></div>`;
      break;
    default:
      codeHtml += `<div title="验证通过" class="status status-warning"><i class="icon icon-warning-circle"></i></div>`;
      break;
  }
  return codeHtml;
}
